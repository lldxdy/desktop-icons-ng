# Korean translation for desktop-icons.
# Copyright (C) 2021 desktop-icons's COPYRIGHT HOLDER
# This file is distributed under the same license as the desktop-icons package.
# JGJeong <jingimem@gmail.com>, 2021.
# Junghee Lee <daemul72@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: desktop-icons master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-29 20:34+0200\n"
"PO-Revision-Date: 2022-04-23 10:08+0900\n"
"Last-Translator: 이정희 <daemul72@gmail.com>\n"
"Language-Team: Korean <jingimem@gmail.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.0.1\n"

#: autoAr.js:87
msgid "AutoAr is not installed"
msgstr ""

#: autoAr.js:88
msgid ""
"To be able to work with compressed files, install file-roller and/or gir-1.2-"
"gnomeAutoAr"
msgstr ""

#: autoAr.js:223
#, fuzzy
msgid "Extracting files"
msgstr "여기 풀기"

#: autoAr.js:240
#, fuzzy
msgid "Compressing files"
msgstr "{0}개 파일 압축하기"

#: autoAr.js:294 autoAr.js:621 desktopManager.js:919 fileItemMenu.js:409
msgid "Cancel"
msgstr "취소"

#: autoAr.js:302 askRenamePopup.js:49 desktopManager.js:917
msgid "OK"
msgstr "확인"

#: autoAr.js:315 autoAr.js:605
msgid "Enter a password here"
msgstr ""

#: autoAr.js:355
msgid "Removing partial file '${outputFile}'"
msgstr ""

#: autoAr.js:374
msgid "Creating destination folder"
msgstr ""

#: autoAr.js:406
msgid "Extracting files into '${outputPath}'"
msgstr ""

#: autoAr.js:436
#, fuzzy
msgid "Extraction completed"
msgstr "여기 풀기"

#: autoAr.js:437
msgid "Extracting '${fullPathFile}' has been completed."
msgstr ""

#: autoAr.js:443
#, fuzzy
msgid "Extraction cancelled"
msgstr "여기 풀기"

#: autoAr.js:444
msgid "Extracting '${fullPathFile}' has been cancelled by the user."
msgstr ""

#: autoAr.js:454
msgid "Passphrase required for ${filename}"
msgstr ""

#: autoAr.js:457
msgid "Error during extraction"
msgstr ""

#: autoAr.js:485
msgid "Compressing files into '${outputFile}'"
msgstr ""

#: autoAr.js:498
msgid "Compression completed"
msgstr ""

#: autoAr.js:499
msgid "Compressing files into '${outputFile}' has been completed."
msgstr ""

#: autoAr.js:503 autoAr.js:510
msgid "Cancelled compression"
msgstr ""

#: autoAr.js:504
msgid "The output file '${outputFile}' already exists."
msgstr ""

#: autoAr.js:511
msgid "Compressing files into '${outputFile}' has been cancelled by the user."
msgstr ""

#: autoAr.js:514
msgid "Error during compression"
msgstr ""

#: autoAr.js:546
msgid "Create archive"
msgstr ""

#: autoAr.js:571
#, fuzzy
msgid "Archive name"
msgstr "파일 이름"

#: autoAr.js:602
msgid "Password"
msgstr ""

#: autoAr.js:618
msgid "Create"
msgstr ""

#: autoAr.js:695
msgid "Compatible with all operating systems."
msgstr ""

#: autoAr.js:701
msgid "Password protected .zip, must be installed on Windows and Mac."
msgstr ""

#: autoAr.js:707
msgid "Smaller archives but Linux and Mac only."
msgstr ""

#: autoAr.js:713
msgid "Smaller archives but must be installed on Windows and Mac."
msgstr ""

#: askRenamePopup.js:42
msgid "Folder name"
msgstr "폴더 이름"

#: askRenamePopup.js:42
msgid "File name"
msgstr "파일 이름"

#: askRenamePopup.js:49
msgid "Rename"
msgstr "이름 바꾸기"

#: dbusUtils.js:66
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr "\"${programName}\"은(는) 바탕 화면 아이콘에 필요합니다"

#: dbusUtils.js:67
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""
"이 기능이 바탕 화면 아이콘에서 작동하려면 시스템에 \"${programName}\"을(를) "
"설치해야 합니다."

#: desktopIconsUtil.js:96
msgid "Command not found"
msgstr "명령을 찾을 수 없습니다"

#: desktopManager.js:229
msgid "Nautilus File Manager not found"
msgstr "노틸러스 파일 관리자를 찾을 수 없습니다"

#: desktopManager.js:230
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr "노틸러스 파일 관리자는 Desktop Icons NG로 작업하는 데 필수입니다."

#: desktopManager.js:881
msgid "Clear Current Selection before New Search"
msgstr "새 검색 전에 현재 선택 지우기"

#: desktopManager.js:921
msgid "Find Files on Desktop"
msgstr "데스크탑에서 파일 찾기"

#: desktopManager.js:986 desktopManager.js:1669
msgid "New Folder"
msgstr "새 폴더"

#: desktopManager.js:990
msgid "New Document"
msgstr "새 문서"

#: desktopManager.js:995
msgid "Paste"
msgstr "붙여넣기"

#: desktopManager.js:999
msgid "Undo"
msgstr "실행 취소"

#: desktopManager.js:1003
msgid "Redo"
msgstr "다시 실행"

#: desktopManager.js:1009
msgid "Select All"
msgstr "모두 선택"

#: desktopManager.js:1017
msgid "Show Desktop in Files"
msgstr "파일에 바탕화면 표시"

#: desktopManager.js:1021 fileItemMenu.js:323
msgid "Open in Terminal"
msgstr "터미널로 열기"

#: desktopManager.js:1027
msgid "Change Background…"
msgstr "배경화면 변경…"

#: desktopManager.js:1038
msgid "Desktop Icons Settings"
msgstr "바탕화면 아이콘 설정"

#: desktopManager.js:1042
msgid "Display Settings"
msgstr "화면 설정"

#: desktopManager.js:1727
msgid "Arrange Icons"
msgstr "아이콘 정렬"

#: desktopManager.js:1731
msgid "Arrange By..."
msgstr "정렬 기준..."

#: desktopManager.js:1740
msgid "Keep Arranged..."
msgstr "정렬 유지..."

#: desktopManager.js:1744
msgid "Keep Stacked by type..."
msgstr "유형별로 스택됨 유지..."

#: desktopManager.js:1749
msgid "Sort Home/Drives/Trash..."
msgstr "집/드라이브/휴지통 정렬..."

#: desktopManager.js:1755
msgid "Sort by Name"
msgstr "이름순으로 정렬"

#: desktopManager.js:1757
msgid "Sort by Name Descending"
msgstr "이름 내림차순으로 정렬"

#: desktopManager.js:1760
msgid "Sort by Modified Time"
msgstr "수정된 시간순으로 정렬"

#: desktopManager.js:1763
msgid "Sort by Type"
msgstr "유형순으로 정렬"

#: desktopManager.js:1766
msgid "Sort by Size"
msgstr "크기순으로 정렬"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:171
msgid "Home"
msgstr "홈"

#: fileItem.js:290
msgid "Broken Link"
msgstr "깨진 링크"

#: fileItem.js:291
msgid "Can not open this File because it is a Broken Symlink"
msgstr "이 파일은 손상된 심볼릭링크이므로 열 수 없습니다"

#: fileItem.js:346
msgid "Broken Desktop File"
msgstr "깨진 바탕화면 파일"

#: fileItem.js:347
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""
"이 .desktop 파일에 오류가 있거나 권한이 없는 프로그램이 있습니다. 실행할 수 "
"없습니다.\n"
"\n"
"<b>파일을 편집하여 올바른 실행 프로그램을 설정합니다.</b>"

#: fileItem.js:353
msgid "Invalid Permissions on Desktop File"
msgstr "바탕화면 파일에 대한 잘못된 권한"

#: fileItem.js:354
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""
"이 .desktop 파일에 잘못된 권한이 있습니다. 마우스 오른쪽 버튼을 클릭하여 속성"
"을 편집 한 다음:\n"

#: fileItem.js:356
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""
"\n"
"<b>\"다른 접근\", \"읽기 전용\" 또는 \"없음\"에서 권한 설정</b>"

#: fileItem.js:359
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""
"\n"
"<b>활성화 옵션, \"파일을 프로그램으로 실행하기 허용\"</b>"

#: fileItem.js:367
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""
"이 .desktop 파일은 신뢰할 수 없으며 실행할 수 없습니다. 시작을 활성화하려면 "
"마우스 오른쪽 버튼을 클릭하고 다음을 수행합니다.\n"
"\n"
"<b>\"시작하기 허용\" 활성화</b>"

#: fileItemMenu.js:132
msgid "Open All..."
msgstr "모두 열기…"

#: fileItemMenu.js:132
msgid "Open"
msgstr "열기"

#: fileItemMenu.js:143
msgid "Stack This Type"
msgstr "이 유형 스택"

#: fileItemMenu.js:143
msgid "Unstack This Type"
msgstr "이 유형 스택 해제"

#: fileItemMenu.js:155
msgid "Scripts"
msgstr "스크립트"

#: fileItemMenu.js:161
msgid "Open All With Other Application..."
msgstr "다른 프로그램으로 모두 열기…"

#: fileItemMenu.js:161
msgid "Open With Other Application"
msgstr "다른 프로그램으로 열기"

#: fileItemMenu.js:167
msgid "Launch using Dedicated Graphics Card"
msgstr "전용 그래픽 카드를 사용하여 실행"

#: fileItemMenu.js:177
msgid "Run as a program"
msgstr "프로그램으로 실행"

#: fileItemMenu.js:185
msgid "Cut"
msgstr "잘라내기"

#: fileItemMenu.js:190
msgid "Copy"
msgstr "복사하기"

#: fileItemMenu.js:196
msgid "Rename…"
msgstr "이름 바꾸기…"

#: fileItemMenu.js:204
msgid "Move to Trash"
msgstr "휴지통으로 옮기기"

#: fileItemMenu.js:210
msgid "Delete permanently"
msgstr "영구 삭제"

#: fileItemMenu.js:218
msgid "Don't Allow Launching"
msgstr "실행을 허용하지 않음"

#: fileItemMenu.js:218
msgid "Allow Launching"
msgstr "실행 허용"

#: fileItemMenu.js:229
msgid "Empty Trash"
msgstr "휴지통 비우기"

#: fileItemMenu.js:240
msgid "Eject"
msgstr "꺼내기"

#: fileItemMenu.js:246
msgid "Unmount"
msgstr "마운트 해제"

#: fileItemMenu.js:258 fileItemMenu.js:265
msgid "Extract Here"
msgstr "여기 풀기"

#: fileItemMenu.js:270
msgid "Extract To..."
msgstr "다른 위치에 풀기…"

#: fileItemMenu.js:277
msgid "Send to..."
msgstr "다음으로 보내기…"

#: fileItemMenu.js:285
#, fuzzy
msgid "Compress {0} folder"
msgid_plural "Compress {0} folders"
msgstr[0] "{0}개 파일 압축하기"

#: fileItemMenu.js:292
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "{0}개 파일 압축하기"

#: fileItemMenu.js:300
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "{0}개 파일 새 폴더로 이동"

#: fileItemMenu.js:309
msgid "Common Properties"
msgstr "공통 속성"

#: fileItemMenu.js:309
msgid "Properties"
msgstr "속성"

#: fileItemMenu.js:316
msgid "Show All in Files"
msgstr "파일에 모두 표시"

#: fileItemMenu.js:316
msgid "Show in Files"
msgstr "파일로 표시"

#: fileItemMenu.js:405
msgid "Select Extract Destination"
msgstr "대상 압축풀기 선택"

#: fileItemMenu.js:410
msgid "Select"
msgstr "선택"

#: fileItemMenu.js:449
msgid "Can not email a Directory"
msgstr "디렉터리는 이메일로 보낼 수 없습니다"

#: fileItemMenu.js:450
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr "선택 파일 중 디렉터리가 포함되어 있어, 먼저 파일로 압축이 필요합니다."

#: preferences.js:74
msgid "Settings"
msgstr "설정"

#: prefswindow.js:46
msgid "Size for the desktop icons"
msgstr "바탕화면 아이콘 크기"

#: prefswindow.js:46
msgid "Tiny"
msgstr "매우 작은 아이콘"

#: prefswindow.js:46
msgid "Small"
msgstr "작은 아이콘"

#: prefswindow.js:46
msgid "Standard"
msgstr "중간 아이콘"

#: prefswindow.js:46
msgid "Large"
msgstr "큰 아이콘"

#: prefswindow.js:47
msgid "Show the personal folder in the desktop"
msgstr "바탕화면에 개인 폴더 표시"

#: prefswindow.js:48
msgid "Show the trash icon in the desktop"
msgstr "바탕화면에 휴지통 표시"

#: prefswindow.js:49 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "바탕화면에 외장 드라이브 표시"

#: prefswindow.js:50 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "바탕화면에 네트워크 드라이브 표시"

#: prefswindow.js:53
msgid "New icons alignment"
msgstr "새 아이콘 정렬 방식"

#: prefswindow.js:54
msgid "Top-left corner"
msgstr "좌측 상단부터"

#: prefswindow.js:55
msgid "Top-right corner"
msgstr "우측 상단부터"

#: prefswindow.js:56
msgid "Bottom-left corner"
msgstr "좌측 하단부터"

#: prefswindow.js:57
msgid "Bottom-right corner"
msgstr "우측 하단부터"

#: prefswindow.js:59 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "새 드라이브를 스크린 반대편에 추가"

#: prefswindow.js:60
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "드래그 앤 드랍 시 드랍 위치 강조 효과"

#: prefswindow.js:61 schemas/org.gnome.shell.extensions.ding.gschema.xml:90
msgid "Use Nemo to open folders"
msgstr "폴더를 여는 Nemo 사용"

#: prefswindow.js:63
msgid "Add an emblem to soft links"
msgstr "소프트 링크에 엠블럼 추가"

#: prefswindow.js:65
msgid "Use dark text in icon labels"
msgstr "아이콘 레이블에 어두운 텍스트 사용"

#. Nautilus options
#: prefswindow.js:71
msgid "Settings shared with Nautilus"
msgstr "노틸러스와 설정 공유"

#: prefswindow.js:90
msgid "Click type for open files"
msgstr "파일 열기 시 클릭 유형"

#: prefswindow.js:90
msgid "Single click"
msgstr "한 번 클릭"

#: prefswindow.js:90
msgid "Double click"
msgstr "두 번 클릭"

#: prefswindow.js:91
msgid "Show hidden files"
msgstr "숨김 파일 표시"

#: prefswindow.js:92
msgid "Show a context menu item to delete permanently"
msgstr "영구적으로 삭제할 컨텍스트 메뉴 항목 표시"

#: prefswindow.js:97
msgid "Action to do when launching a program from the desktop"
msgstr "바탕화면에서 프로그램을 실행할 때 수행할 작업"

#: prefswindow.js:98
msgid "Display the content of the file"
msgstr "파일의 내용 표시"

#: prefswindow.js:99
msgid "Launch the file"
msgstr "파일 실행"

#: prefswindow.js:100
msgid "Ask what to do"
msgstr "어떤 작업 수행할 지 선택"

#: prefswindow.js:106
msgid "Show image thumbnails"
msgstr "이미지 마중그림 표시"

#: prefswindow.js:107
msgid "Never"
msgstr "보지 않음"

#: prefswindow.js:108
msgid "Local files only"
msgstr "로컬 파일만"

#: prefswindow.js:109
msgid "Always"
msgstr "항상"

#: showErrorPopup.js:40
msgid "Close"
msgstr "닫기"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "아이콘 크기"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "바탕 화면 아이콘의 크기를 지정합니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "개인 폴더 표시"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "바탕화면에 개인 폴더를 표시합니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "휴지통 아이콘 표시"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "바탕화면에 휴지통 아이콘을 표시합니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr "새 아이콘 배치할 코너"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "아이콘 배치를 시작할 모서리를 설정합니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "컴퓨터에 연결된 디스크 드라이브를 표시합니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr "데스크탑에 탑재된 네트워크 볼륨을 표시합니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr "바탕화면에 드라이브 및 볼륨을 추가할 때 화면의 반대쪽에 추가합니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "끌어서 놓는 동안 대상 위치에 사격형 표시"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"끌어서 놓기 작업을 수행할 때, 격자에서 아이콘이 배치될 위치를 반투명 사각형"
"을 표시합니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr "특수 폴더 정렬 - 홈/휴지통 드라이브 입니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""
"바탕화면에 아이콘을 정렬할 때 홈, 휴지통 및 마운트된 네트워크 또는 외장 드라"
"이브의 위치를 정렬하고 변경합니다"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr "아이콘 정렬 유지"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr "항상 마지막 정렬 순서로 아이콘 정렬 유지"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr "순서 정렬"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr "이 속성으로 정렬된 아이콘"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr "아이콘 스택됨 유지"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr "항상 아이콘 스택됨 유지, 유사한 유형이 그룹화됩니다"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr "스택하지 않을 파일 유형"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr "문자열 유형의 배열, 이러한 유형의 파일을 스택하지 않습니다"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:91
msgid "Use Nemo instead of Nautilus to open folders."
msgstr "노틸러스 대신 Nemo를 사용하여 폴더를 엽니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:95
msgid "Add an emblem to links"
msgstr "링크에 엠블럼 추가"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:96
msgid "Add an emblem to allow to identify soft links."
msgstr "소프트 링크를 식별할 수 있도록 엠블럼을 추가합니다."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:100
msgid "Use black for label text"
msgstr "레이블 텍스트에 검정색 사용"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:101
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""
"레이블 텍스트를 흰색 대신 검정색으로 칠합니다. 밝은 배경을 사용할 때 유용합니"
"다."

#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr ""
#~ "Desktop Icons NG 설정을 보려면 바탕화면에서 우클릭하여 마지막 아이템 '바탕"
#~ "화면 아이콘 설정'을 선택하세요."

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "“{0}”을 실행하시겠습니까? 혹은 내용을 표시하겠습니까?"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "“{0}”는 실행가능한 텍스트 파일입니다."

#~ msgid "Execute in a terminal"
#~ msgstr "터미널로 실행"

#~ msgid "Show"
#~ msgstr "보기"

#~ msgid "Execute"
#~ msgstr "실행"

#~ msgid "New folder"
#~ msgstr "새 폴더"

#~ msgid "Delete"
#~ msgstr "삭제"

#~ msgid "Error while deleting files"
#~ msgstr "파일 삭제 중 오류가 발생하였습니다."

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "파일들을 영구 삭제하시겠습니까?"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "파일을 삭제하면, 영구적으로 잃게 될 것입니다."
